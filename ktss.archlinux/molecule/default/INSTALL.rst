*******
Delegated driver installation guide
*******

Requirements
============

The playbooks require kewlfft.aur.
For testing, use pip to acquire molecule itself and the podman driver.
Creation of python virtual environments are recommended.
Git
Ansible
Connection to the internet

Install
=======

* Have a basic configuration of internet, as well as git and ansible installed
* Clone the system configuration using ``git clone https://bitbucket.org/ktss/ansible-archlinux``
* Clone the user configuration using ``git clone https://bitbucket.org/ktss/ansible-user``
* Run the system configuration as root.
* Modify the default user in the defaults folder in the role and run the user
  configuration as root too (this prevents having the additional *.ansible*
  directory in your home).
* The system configuration itself becomes idempotent because a play was created
  to keep pulling the system configuration, user settings however should be
  managed manually.
